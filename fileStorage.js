var fs = require("fs"),
	encoding = "utf8",
	path = "/tmp/storage.dat";

module.exports = {
	get: function(key, callback){
		fs.readFile(path, encoding, function(err, data){

			if(!data){
				return callback(err, null);
			}

			var keyValues = {},
				split = data.split("\n"),
				len = split.length;

			for(var i=0; i<len; ++i){
				//pair is a key=value
				var pair = split[i].split("=");
				if(pair[0] === key){
					return callback(err, pair[1]);
				}
			}

			callback(err, null);
		});
	},
	set: function(keyValueObj, callback){
		var keyValues = [];
		for(var key in keyValueObj){
			keyValues.push(key + "=" + keyValueObj[key] +  "\n");
		}

		fs.appendFile(path,  keyValues.join(""), encoding, function(err){
			callback(err);
		});
	}
};