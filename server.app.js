"use strict";

var port = 4000,
	express = require("express"),
	app = express(),
	storage = require("./fileStorage.js");
	//storage = require("./memoryStorage.js");

app.get("/get", function(req, res){
	storage.get(req.query.key, function(err, value){
		if(err){
			console.log(err);
			return res.sendStatus(500);
		}

		if(!value){
			return res.sendStatus(404);
		}
		
		res.send(value);
	});
});

app.get("/set", function(req, res){
	storage.set(req.query, function(err){
		if(err){
			res.sendStatus(500);
		}
		else{
			res.sendStatus(200);
		}
		
	});
	
});

app.listen(port, function(){
	console.log("Listening on port", port);
});