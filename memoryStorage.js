"use strict";

var storage = {};

module.exports = {
	get: function(key, callback){
		callback(null, storage[key]);
	},
	set: function(keyValueObj, callback){
		for(var key in keyValueObj){
			storage[key] = keyValueObj[key];
		}
		
		callback(null);
	}
};